$(document).ready(function () {
  /* Filtros  Comparador Desktop */
  var findClassactive = $(".btn-ag-deposito").parent().find(".active");
  var Count = findClassactive.length;
  var limit = 3;
  var min = 0;
  $(".bbest-btn-filter-toggle li").on("click", "a", function (e) {
    e.preventDefault();
    if ($("a.active").length >= limit) {
      if ($(this).hasClass("active")) {
        $(this).toggleClass("active");
      }
    } else {
      $(this).toggleClass("active");
    }

    /* Desativar Conta */
    if (!$(this).hasClass("active")) {
      if ($("a.active").length == min) {
      }

      if ($("a.active").length > min) {
        $("#" + $(this).attr("data-contas")).fadeOut("slow");
      }
    }

    /* Ativar Card Conta */
    if ($(this).hasClass("active")) {
      $("#" + $(this).attr("data-contas")).fadeIn("slow");
    }

    var findClassactive = $(".btn-ag-deposito").parent().find(".active");
    var Count = findClassactive.length;
    if ($("a.active").length == min) {
      $(this).addClass("active");
      $(".alert-box span").text(
        "Deverá ter pelo menos duas contas selecionadas para comparar."
      );
    } else if ($("a.active").length == limit) {
      $(".alert-box .content").css("opacity", "1");
      $(".alert-box span").text(
        "Por favor, retire uma conta da lista, para poder adicionar uma outra ao comparador."
      );
    }
  });

  /* Fim Filtros Comparador  Desktop*/

  /* Filtros Mobile */
  var limitM = 2;
  var minM = 1;
  /* Select Left */
  $("#depositos").on("change", function (e) {
    e.preventDefault();
    $(".alert-box span").text("Selecione mais uma conta para comparar");
    $(".alert-box .content").css("background", "#FF3333");
    $(".alert-box span").css("color", "#fff");

    var SelectLeft = $("#depositos").children(":selected").attr("data-contas");
    var SelectRight = $("#depositos02")
      .children(":selected")
      .attr("data-contas");
    console.log("tyeste");
    if (SelectRight == "novos-clientes" || SelectRight == "contadigital") {
      $(".container-depositos-a-prazo .row .col-6").fadeOut("slow");
      $(".container-ag  .row .col-6").fadeOut("slow");

      $("#" + $(this).children(":selected").attr("data-contas")).fadeIn("slow");
    } else {
      $(".container-depositos-a-prazo .row .col-6").fadeOut("slow");
      $(".container-ag .row .col-6").fadeOut("slow");
      $("#" + $(this).children(":selected").attr("data-contas")).fadeIn("slow");
      $("#" + SelectRight).fadeIn("slow");
    }

    $("#depositos02 option").each(function () {
      if (SelectLeft == $(this).val()) {
        $(this).css("display", "none");
      }
    });
  });

  /* Select Right */
  $("#depositos02").on("change", function (e) {
    e.preventDefault();
    $(".alert-box span").text("Selecione mais uma conta para comparar na");
    $(".alert-box .content").css("background", "#FF3333");
    $(".alert-box span").css("color", "#fff");

    var SelectLeft = $("#depositos").children(":selected").attr("data-contas");
    var SelectRight = $("#depositos02")
      .children(":selected")
      .attr("data-contas");

    if (SelectLeft == "novos-clientes" || SelectLeft == "contadigital") {
      $(".container-depositos-a-prazo .row .col-6").fadeOut("slow");
      $(".container-ag  .row .col-6").fadeOut("slow");
      $("#" + $(this).children(":selected").attr("data-contas")).fadeIn("slow");
    } else {
      $(".container-depositos-a-prazo .row .col-6").fadeOut("slow");
      $(".container-ag  .row .col-6").fadeOut("slow");
      $("#" + SelectLeft).fadeIn("slow");
      $("#" + $(this).children(":selected").attr("data-contas")).fadeIn("slow");
    }

    $("#depositos option").each(function () {
      if (SelectRight == $(this).val()) {
        $(this).css("display", "none");
      }
    });
  });
  /*End Filtros Mobile*/

  /* Slider leiloes */
  $(".container-slider-leiloes").slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: false,
    arrows: false,
    variableWidth: true,
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
        },
      },
    ],
  });
  /* Fim Slider leiloes */

  /* Reload */
  function refreshPage() {
    if (window.matchMedia("(max-width: 768.98px)").matches) {
      /* Ag depositos */
      $("#fixo").css("display", "blcok");
      $("#novos-clientes").css("display", "block");

      $("#a-medida").css("display", "none");
      $("#moeda-estrangeira").css("display", "none");
      $("#conta-popanca").css("display", "none");

      /* Ag contas */
      $("#contadigital").css("display", "blcok");
      $("#blinkin").css("display", "blcok");
      $("#contaordenado").css("display", "none");
      $("#besttrading").css("display", "none");
      $("#contadigitalmais").css("display", "none");
      $("#startplus").css("display", "none");
      $("#nonresident").css("display", "none");

      $(window).resize(function () {
        location.reload();
      });
    } else {
      /* Ag deposditos */
      $("#novos-clientes").css("display", "block");
      $("#fixo").css("display", "block");
      $("#a-medida").css("display", "block");
      $("#moeda-estrangeira").css("display", "none");
      $("#conta-popanca").css("display", "none");
      /* Ag Contas */
      $("#contadigital").css("display", "blcok");
      $("#blinkin").css("display", "blcok");
      $("#contaordenado").css("display", "blcok");
      $("#besttrading").css("display", "none");
      $("#contadigitalmais").css("display", "none");
      $("#startplus").css("display", "none");
      $("#nonresident").css("display", "none");
    }
  }
  window.addEventListener("resize", refreshPage, true);
  refreshPage();
  /*Fim Reload */

  /* CARD STICKY TOP WHEN SCROLL DOWN */

  const card = document.querySelector(".card-top-depositos");
  const topcardTop = card.offsetTop;

  function fixCard() {
    console.log(window.scrollY);

    if ($(window).width() > 1300) {
      /* alert("2000"); */

      if (window.scrollY > 1600) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "#fff");
      }

      if (window.scrollY > 1800) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "none");
      }

      /*contas*/

      if (window.scrollY > 1450) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#fff"
        );
      }
      if (window.scrollY > 1600) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "none"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "211px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "211px"
        );
        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "525px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "348px"
        );
      }
    } else if ($(window).width() > 1200 && $(window).width() < 1300) {
      /*depositos*/
      if (window.scrollY > 1450) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "#fff");
      }

      if (window.scrollY > 1800) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "none");
      }

      /*contas*/

      if (window.scrollY > 1450) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#fff"
        );
      }

      if (window.scrollY > 1600) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "none"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "211px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "211px"
        );
        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "525px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "348px"
        );
      }
    } else if ($(window).width() > 992 && $(window).width() < 1200) {
      /*  alert("992"); */

      /*depositos*/
      if (window.scrollY > 1100) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "#fff");
      }

      if (window.scrollY > 1200) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "none");
      }

      /*contas*/

      if (window.scrollY > 1200) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#fff"
        );
      }

      if (window.scrollY > 1300) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "none"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "195px");
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "195px"
        );
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );

        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "365px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "348px"
        );
      }
    } else if ($(window).width() > 768) {
      /*alert("768");*/

      /*depositos*/
      if (window.scrollY > 1100) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "#fff");
      }

      if (window.scrollY > 1200) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "none");
      }

      /*contas*/

      if (window.scrollY > 1200) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#fff"
        );
      }

      if (window.scrollY > 1300) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "none"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "195px");
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "195px"
        );

        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );

        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "365px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "291px"
        );
      }
    } else if ($(window).width() > 490) {
      /*depositos*/
      if (window.scrollY > 1190) {
        $(".cards-filters .bbest-btn-selector .content-select").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-selector ").css("background", "#dfeaf2");
      } else {
        $(".cards-filters .bbest-btn-selector .content-select").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-selector ").css("background", "#fff");
      }

      if (window.scrollY > 1220) {
        $(".cards-filters .bbest-btn-selector .content-select").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-selector ").css("background", "none");
      }

      /*contas*/

      if (window.scrollY > 1190) {
        $(".cards-filters-contas .bbest-btn-selector .content-select").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-selector ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-selector .content-select").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-selector ").css(
          "background",
          "#fff"
        );
      }

      if (window.scrollY > 1220) {
        $(".cards-filters-contas .bbest-btn-selector .content-select").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-selector ").css(
          "background",
          "none"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "195px");
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "195px"
        );

        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );

        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "365px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "291px"
        );
      }
    } else if ($(window).width() > 300) {
      /*  alert("300"); */
      /*depositos*/
      if (window.scrollY > 1250) {
        $(".cards-filters .bbest-btn-selector .content-select").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-selector ").css("background", "#dfeaf2");
      } else {
        $(".cards-filters .bbest-btn-selector .content-select").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-selector ").css("background", "#fff");
      }

      if (window.scrollY > 1350) {
        $(".cards-filters-contas .bbest-btn-selector .content-select").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-selector ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-selector .content-select").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-selector ").css(
          "background",
          "#fff"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "225px");
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "225px"
        );

        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );

        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "365px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "291px"
        );
      }
    } else if ($(window).width() < 300) {
      /*depositos*/
      if (window.scrollY > 1450) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "#fff");
      }

      if (window.scrollY > 1800) {
        $(".cards-filters .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters .bbest-btn-filter-toggle ").css("background", "none");
      }

      /*contas*/

      if (window.scrollY > 1450) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#dfeaf2"
        );
      } else {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "1"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "#fff"
        );
      }

      if (window.scrollY > 1600) {
        $(".cards-filters-contas .bbest-btn-filter-toggle .content").css(
          "opacity",
          "0"
        );
        $(".cards-filters-contas .bbest-btn-filter-toggle ").css(
          "background",
          "none"
        );
      }

      if (window.scrollY > 390) {
        $(".card-top-depositos .content").css("opacity", "0");
        $(".card-top-depositos .content-sticky").css("opacity", "1");
        $(".cards-filters .card-top-depositos").css("min-height", "195px");
        $(".cards-filters .card-top-depositos .content-sticky").css(
          "min-height",
          "195px"
        );

        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "168px"
        );

        $(".cards-filters-contas .card-top-depositos .content-sticky").css(
          "min-height",
          "168px"
        );
      } else {
        $(".card-top-depositos .content").css("opacity", "1");
        $(".card-top-depositos .content-sticky").css("opacity", "0");
        $(".cards-filters .card-top-depositos").css("min-height", "365px");
        $(".cards-filters-contas .card-top-depositos").css(
          "min-height",
          "291px"
        );
      }
    }
  }

  window.addEventListener("scroll", fixCard);
});
